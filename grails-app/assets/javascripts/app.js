/**
 * Created by Hafiz on 7/25/2017.
 */
angular.module('tvMaidApp', [])
    .controller('tvMaidCtrl', ["$scope", "tvMaidService", "$window", function ($scope, tvMaidService, $window) {
        activate();

        function activate() {
        }

        $scope.selectedChannels = []; //[ { name: "AFMAG", id: "102"}, { name: "MITV", id: "400"} ];

        $scope.addSelectedChannel = function(){
            var channelsDropdown = $('select[name=channels]').first()
            var id = channelsDropdown.val();

            if(!id){
                alert("Please, select a valid channel")
                return;
            }

            var lookup = {};
            for (var i = 0, len = $scope.selectedChannels.length; i < len; i++) {
                lookup[$scope.selectedChannels[i].id] = $scope.selectedChannels[i];
            }

            if(lookup[id]){
                alert("Channel has already been selected")
                return;
            }

            var newChannel = { name: $('#'+channelsDropdown[0].id+' :selected').text(), id: id }
            $scope.selectedChannels.push(newChannel)
        }
    }])
    .factory('tvMaidService', [function () {
        var service = {
            cake: "eatable"
        };
        return service;
    }]);