package tvmaid

class DstvController {
    def dstvService

    def getPackage(String packageName, String countryName) {
        def theCountry = DstvCountry.findByUrlFriendlyName(countryName)
        def thePackage = theCountry.countryPackages.find { it.urlFriendlyName == packageName }
        def theCategories = dstvService.categorizeChannels(thePackage.channels)
        respond([theCountry: theCountry, thePackage: thePackage, theCategories: theCategories])
    }

    def getChannel(String channelName, int channelNumber, String countryName) {
        def country = DstvCountry.findByUrlFriendlyName(countryName)
        /* Returning packagesWithChannel, theChannel, theCountry, similarChannels*/

        if (country) {
            def theChannel = DstvChannel.findWhere([number: channelNumber, urlFriendlyName: channelName, languageCode: country.defaultLanguageCode])
            if (theChannel) {
                /* TODO: Search for the packageName amongst the packages belongs in */

                def packagesWithChannel = theChannel.packages.findAll {
                    it.country == country
                }.sort {
                    it.price
                }

                def similarChannels = country.countryChannels.findAll {
                    it.category == theChannel.category
                }

                similarChannels.remove(theChannel)
                respond([theChannel: theChannel, similarChannels: similarChannels, channelPackages: packagesWithChannel, theCountry: country])
            } else {
                /* TODO: Redirect to all country channels page with appropriate flash message */
            }
        }
    }

    def index() {
        def allCountries = DstvCountry.list()
        respond([theCountries: allCountries.sort { it.name }])
    }

    def channelInPackage(String channelName, int channelNumber, String packageName, String countryName) {
        def country = DstvCountry.findByUrlFriendlyName(countryName)
        if (country) {
            def theChannel = DstvChannel.findWhere([number: channelNumber, urlFriendlyName: channelName, languageCode: country.defaultLanguageCode])
            if (theChannel) {
                /* TODO: Search for the packageName amongst the packages belongs in */

                def packagesWithChannel = theChannel.packages.findAll {
                    it.country == country
                }.sort {
                    it.price
                }

                def searchPackage = DstvPackage.findWhere(urlFriendlyName: packageName, country: country)

                def thePackage = packagesWithChannel.contains(searchPackage)

                respond([searchResponse: thePackage ? "YES" : "NO", thePackage: searchPackage, theChannel: theChannel, channelPackages: packagesWithChannel, country: country])
            } else {
                /* TODO: Redirect to all country channels page */
            }
        }
    }

    def refreshPackages(String username, String password) {
        if(username == System.env.DSTV_REFRESH_KEY && password == System.env.DSTV_REFRESH_SECRET){
            def countries = DstvCountry.list()
            def refreshers = []
            def doneCount = 0

            countries.each { country ->
                def packageNames = dstvService.getCountryPackageNames(country)
                packageNames.each { String theName ->
                    def refresher = new DstvPackageRefresher(country, theName)
                    new Thread(refresher).start()
                    refreshers.add(refresher)
                }

                while (doneCount < refreshers.size()) {
                    for (DstvPackageRefresher refresher in refreshers) {
                        if (refresher.done && !refresher.saved) {
                            if (refresher.result) {
                                log.info("Start: Saving packages for $refresher.packageName $country")
                                dstvService.savePackage(refresher.packageName, refresher.result, refresher.country)
                                log.info("Done: Saving packages for $refresher.packageName $country")
                            }

                            refresher.saved = true
                            doneCount++
                        }
                    }
                }
            }
            log.info("Package refresh done for ${refreshers.size()} channels")
        }
        redirect(uri: '/')
    }

    def packageSearch(String countryName) {
        if (request.get) {
            def theCountry = DstvCountry.findByUrlFriendlyName(countryName)
            if (theCountry) {
                def thePackages = theCountry.countryPackages
                def theChannels = theCountry.countryChannels
                respond([packageList: thePackages, country: theCountry, channelList: theChannels])
            } else {
                redirect action: 'index'
            }
        } else {
            /** Retrieve the package
             * For each channel:
             * 1. check if it exists in this package
             *  > Yes: Add yes against this channel and generate link to the individual page to see other packages that have this channel
             *  > No: Add no to the results and generate link to the individual page to see the packages with this channel
             */
            /* IT IS POST. Take it or leave it! */
            def packageId = params["package"]
            if(!packageId){
                flash.message = "Please, select a valid package"
                redirect url: createLink(mapping: 'packageSearch', params: [countryName: countryName])
                return
            }
            packageId = packageId as long
            def channelIds = params.list("selectChannels")
            def result = [] /* This will be a list of maps */
            def thePackages = null
            def theChannels = null

            def thePackage = DstvPackage.get(packageId)
            def theCountry = DstvCountry.findByUrlFriendlyName(countryName)

            if (thePackage && theCountry) {
                channelIds.each { it ->
                    def theChannel = DstvChannel.get(it as long)
                    if (theChannel) {
                        def channelStatus = [
                                channelNumber: theChannel.number,
                                channelName  : theChannel.name,
                                status       : thePackage.channels.contains(theChannel) ? 'YES' : 'NO',
                                urlBuilder   : [
                                        packageName  : thePackage.urlFriendlyName,
                                        channelName  : theChannel.urlFriendlyName,
                                        channelNumber: theChannel.number,
                                ]
                        ]
                        result.add(channelStatus)
                    }
                }

                thePackages = theCountry.countryPackages
                theChannels = theCountry.countryChannels
            }

            respond([channelPackageStatuses: result, packageName: thePackage.name, country: theCountry, packageList: thePackages, channelList: theChannels])
        }

    }

    def getCountry(String countryName) {
        /* TODO: Return packages and channels */

        def theCountry = DstvCountry.findByUrlFriendlyName(countryName)
        if (theCountry) {
            def thePackages = theCountry.countryPackages
            def theChannels = theCountry.countryChannels
            respond([thePackages: thePackages.sort {
                it.price
            }, theCountry       : theCountry, theChannels: theChannels])
        }
    }

    /*def tempGenerateUrls(){
        dstvService.generateChannelInCountryPackageUrls("https://dstvsage.herokuapp.com")
    }*/

}
