package tvmaid


class DstvInterceptor {
    def userAgentIdentService

    boolean before() {
        request.setProperty('startTime', new Date())
        true
    }

    boolean after() {
        RequestLoggerActor actor = new RequestLoggerActor ()
        actor.start()
        actor.send(
                clientIp: request.getHeader('x-forwarded-for') ?: request.getHeader('client-ip') ?: request.remoteAddr,
                uri: request.requestURI,
                startTime: request.getProperty('startTime') as Date,
                responseCode: response.status,
                userAgent: request.getHeader('user-agent'),
                isMobile: userAgentIdentService.mobile
        )
        true
    }

    void afterView() {
        // no-op
    }
}
