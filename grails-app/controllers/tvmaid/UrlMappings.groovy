package tvmaid

class UrlMappings {

    static mappings = {
        /* Grails regex URL mapping */
        name countryPackagesAndChannels:"/dstv-$countryName-packages-channels"{
            controller = "dstv"
            action = "getCountry"
            constraints {
                countryName(matches: /[A-Za-z\-]+/)
            }
        }

        /* Grails regex URL mapping */
        name packageChannelsAndInfo:"/dstv-$packageName-channels-price-$countryName"{
            controller = "dstv"
            action = "getPackage"
            constraints {
                packageName(matches: /[A-Za-z0-9\-]+/)
                countryName(matches: /[A-Za-z\-]+/)
            }
        }

        /* Grails regex URL mapping */
        name channelPackagesAndInfo:"/dstv-$channelName-channel-num-$channelNumber-packages-info-$countryName"{
            controller = "dstv"
            action = "getChannel"
            constraints {
                channelName(matches: /[A-Za-z0-9\-]+/)
                channelNumber(matches: /[0-9]{1,4}/)
                countryName(matches: /[A-Za-z\-]+/)
            }
        }

        /* Grails regex URL mapping */
        name packagesWithChannel: "/dstv-$countryName-packages-with-$channelName-channel-$channelNumber"{
            controller = "dstv"
            action = "getChannel"
            constraints {
                channelName(matches: /[A-Za-z0-9\-]+/)
                channelNumber(matches: /[0-9]{1,4}/)
                countryName(matches: /[A-Za-z\-]+/)
            }
        }

        name channelInPackage: "/does-dstv-$packageName-have-$channelName-channel-num-$channelNumber-$countryName"{
            controller = "dstv"
            action = "channelInPackage"
            constraints {
                channelName(matches: /[A-Za-z0-9\-]+/)
                channelNumber(matches: /[0-9]{1,4}/)
                countryName(matches: /[A-Za-z\-]+/)
                packageName(matches: /[A-Za-z0-9\-]+/)
            }
        }

        "/dstv-refresh-packages"{
            controller = "dstv"
            action = "refreshPackages"
        }

        name packageSearch: "/does-dstv-$countryName-package-have-channels"{
            controller = "dstv"
            action = "packageSearch"
            constraints {
                countryName(matches: /[A-Za-z\-]+/)
            }
        }


        name robots: "/robots.txt" (view: "/robots")
        name sitemap: "/sitemap.txt" (view: "/sitemap")
        name about: "/about" (view: "/about")
        name howItWorks: "/how-it-works" (view: "/howItWorks")

        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }


        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: "dstv")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
