package nse

class StockCompany {
    String name
    String symbol
    String sector
    String registrar = "UNKNOWN"
    int externalId
    Date dateCreated
    Date lastUpdated

    static constraints = {
        symbol unique: true
        externalId nullable: true
        name nullable: true
    }
}
