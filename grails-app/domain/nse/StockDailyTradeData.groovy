package nse

class StockDailyTradeData {
    StockCompany stockCompany
    BigDecimal previousClosePrice
    BigDecimal openingPrice
    BigDecimal lowPrice
    BigDecimal highPrice
    BigDecimal closePrice
    BigDecimal priceChange
    BigDecimal percentageChange
    int tradeCount
    Long volumeTraded
    BigDecimal valueTraded
    String market
    Date tradeDate
    Date dateCreated
    Date lastUpdated

    static constraints = {
        stockCompany unique: "tradeDate"
        market nullable: true
        openingPrice nullable: true
        highPrice nullable: true
        lowPrice nullable: true
        priceChange nullable: true
        percentageChange nullable: true
        volumeTraded nullable: true
        valueTraded nullable: true
    }
}
