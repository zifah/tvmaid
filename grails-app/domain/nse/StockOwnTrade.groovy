package nse

class StockOwnTrade {
    StockPortfolioItem tradedItem
    int volume
    /* Autopopulate this with the stock's last closing price */
    BigDecimal unitPrice
    BigDecimal netIncome
    BigDecimal totalPurchasePrice
    Date tradeDate
    Date dateCreated
    Date lastUpdated
    String narration
    /* Trade types: CorrectionAdd, CorrectionReduce, Sell, Buy */
    TradeType tradeType

    static constraints = {
        netIncome nullable: true
        totalPurchasePrice nullable: true
    }
}
