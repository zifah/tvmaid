package nse

class StockPortfolio {
    User user
    boolean isActive = true
    String name
    String description
    String accountNumber
    String clearingHouseNumber
    String cscsAccountNumber
    String stockBroker
    String postalAddress
    Date dateCreated
    Date lastUpdated

    static mapping = {
        description sqlType: "text"
        postalAddress sqlType: "text"
    }

    static constraints = {
        stockBroker nullable: true
        description nullable: true
        clearingHouseNumber nullable: true
        cscsAccountNumber nullable: true
    }
}
