package nse

class StockPortfolioItem {
    StockPortfolio portfolio
    StockCompany company
    String accountNumber
    int volume
    BigDecimal purchasePrice
    Date purchaseDate
    String edividentAccountName
    String edividendAccountNumber
    String edividendAccountBank
    Date dateCreated
    Date lastUpdated

    static constraints = {
        purchaseDate nullable: true
        edividendAccountBank nullable: true
        edividendAccountNumber nullable: true
        edividentAccountName nullable: true
    }
}
