package nse

class User {

    String username
    String firstName
    String lastName
    String emailAddress

    static constraints = {
        username unique: true
        emailAddress nullable: true
        firstName nullable: true
        lastName nullable: true
    }
}
