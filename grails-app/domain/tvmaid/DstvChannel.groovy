package tvmaid

class DstvChannel {
    int number
    String name
    String urlFriendlyName
    String category
    String description
    String mobileLogo
    String webLogo
    String languageCode

    static  belongsTo = DstvPackage
    static hasMany = [packages:DstvPackage]

    static mapping = {
        description sqlType: 'text'
    }

    static constraints = {
        name unique: ['number', 'languageCode']
        category nullable: true
        description nullable: true
        mobileLogo nullable: true
        webLogo nullable: true
    }

    String toString(){
        "$name"
    }

    String getDescriptionSanitized(){
        "${description?.replaceAll("<(.|\n)*?>", '')}"
    }
}
