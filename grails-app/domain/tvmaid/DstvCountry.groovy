package tvmaid

class DstvCountry {
    String name
    String urlFriendlyName
    String twoLetterCode
    String currency
    String defaultLanguageCode

    static constraints = {
        defaultLanguageCode nullable: true
        name unique: true
        twoLetterCode unique: true
    }

    static mapping = {
        defaultLanguageCode defaultValue: "'en'"
    }

    String toString(){
        "$name"
    }

    /**
     * A list of country packages
     * @return
     */
    def getCountryPackages(){
        DstvPackage.findAllByCountry(this)
    }

    /**
     * A list of country channels ordered by name
     */
    def getCountryChannels(){
        DstvChannel.where {
            packages {
                country == this
            }
            order('name')
        }.list()
    }
}
