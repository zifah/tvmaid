package tvmaid

class DstvPackage {
    BigDecimal price
    String name
    /* A version of the package name that is URL safe. Contains hyphens (in place of space), letters and numbers */
    String urlFriendlyName
    String url
    String description
    String thumbnail
    String logo
    String slug
    DstvCountry country
    Date lastRefreshDate

    static hasMany = [channels:DstvChannel]

    static mapping = {
        description sqlType: 'text'
    }

    static constraints = {
        name unique: 'country'
        slug unique: 'country'
        price nullable: true
        url nullable: true
        description nullable: true
        thumbnail nullable:  true
        logo nullable: true
        urlFriendlyName nullable: true
        lastRefreshDate nullable: true
        slug nullable: true
    }

    String toString(){
        "$name"
    }
}
