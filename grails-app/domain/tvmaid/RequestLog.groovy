package tvmaid

class RequestLog {
    String theUrl
    String clientIp
    Date startTime
    Date dateCreated
    int responseCode
    int latencyMs
    String userAgent
    boolean isMobile

    static constraints = {
    }

    static mapping = {
        userAgent length: 2000
    }
}
