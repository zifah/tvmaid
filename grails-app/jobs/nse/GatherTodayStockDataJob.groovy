package nse

class GatherTodayStockDataJob {
    DataGathererService dataGathererService
    static triggers = {
        cron cronExpression: "0 0 17 * * ?" /* Once a day at 5pm everyday */
    }

    def execute() {
        // execute job
        dataGathererService.collectTodaysTradeData(new Date().clearTime())
    }
}
