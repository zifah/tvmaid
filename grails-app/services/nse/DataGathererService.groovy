package nse

import grails.converters.JSON
import grails.transaction.Transactional
import org.grails.web.json.JSONObject
import utils.WebUtility

class DataGathererService {

    def STOCK_PRICES_ENDPOINT = "http://www.nse.com.ng/rest/api/statistics/equities/?market=&sector=&orderby=&pageSize=1000&pageNo=0"
    def ADMINISTRATOR_EMAIL = "hoadewuyi@gmail.com"
    def NORMAL_MESSAGE_TITLE = "NSE Agent Notification"
    def ERROR_MESSAGE_TITLE = "NSE Agent ERROR Notification"

    /**
     * Make a call to the data source, parse the results and save
     *{"$id": "67",
     "Id": 7,
     "Symbol": "GUARANTY",
     "PrevClosingPrice": 40,
     "OpeningPrice": 40.1,
     "HighPrice": 40.1,
     "LowPrice": 39.9,
     "ClosePrice": 40,
     "Change": 0,
     "PercChange": 0.2,
     "Trades": 352,
     "Volume": 34858874,
     "Value": 1394220081.31,
     "Market": "Main Board",
     "Sector": "FINANCIAL SERVICES",
     "Company2": "GUARANTY "}*/
    @Transactional
    def collectTodaysTradeData(Date tradeDate) {
        log.info("Stock data gathering job triggered")
        def todaysDate = new Date().clearTime()
        if (tradeDate < todaysDate) {
            notifyAdministrator("Tried to get market data for $tradeDate on $todaysDate. Now exiting", true)
            return
        }

        def tradeDataRaw

        try {
            tradeDataRaw = WebUtility.makeGetRequest(STOCK_PRICES_ENDPOINT)
        } catch (Exception ex) {
            log.error("Error getting today's stock data. Retrying in 10 minutes: $ex.message")
            Thread.sleep(10 * 60 * 1000)
            return collectTodaysTradeData(tradeDate)
        }

        if (tradeDataRaw) {
            try {
                def tradeData = JSON.parse(tradeDataRaw)
                def savedData = []


                tradeData.each { JSONObject dataRow ->
                    /* create stockCompany if not exists */
                    StockCompany company = StockCompany.findWhere(symbol: dataRow.Symbol.trim()) ?: new StockCompany(symbol: dataRow.Symbol.trim())
                    company.externalId = dataRow.Id
                    company.sector = dataRow.Sector?.trim()
                    company.save(failOnError: true)

                    /* save today's trade data against that stockCompany */
                    savedData << new StockDailyTradeData(stockCompany: company, lowPrice: dataRow.LowPrice,
                            highPrice: dataRow.HighPrice, previousClosePrice: dataRow.PrevClosingPrice,
                            openingPrice: dataRow.OpeningPrice, closePrice: dataRow.ClosePrice, priceChange: dataRow.Change,
                            percentageChange: dataRow.PercChange, tradeCount: dataRow.Trades, volumeTraded: dataRow.Volume,
                            valueTraded: dataRow.Value, market: dataRow.Market.trim(), tradeDate: tradeDate).save(failOnError: true)
                }
                notifyAdministrator("NSE trade data was saved successfully for $tradeDate", false)
            } catch (Exception ex) {
                notifyAdministrator("An error occurred while trying to save today's market data: $ex.message", true)
            }
        } else {
            notifyAdministrator("The stock market data response was empty. No trade data was saved today", true)
        }
    }

    def notifyAdministrator(String message, boolean isError) {
        isError ? log.error(message) : log.info(message)
        sendEMail(message, ADMINISTRATOR_EMAIL, isError ? ERROR_MESSAGE_TITLE : NORMAL_MESSAGE_TITLE)
    }

    def sendEMail(String messageBody, String recipient, String title) {
        /* TODO: Send a message */
    }
}
