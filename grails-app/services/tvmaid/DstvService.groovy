package tvmaid

import grails.converters.JSON
import grails.transaction.Transactional
import groovy.time.TimeCategory
import org.apache.catalina.util.URLEncoder
import org.grails.web.json.JSONObject
import org.springframework.orm.ObjectOptimisticLockingFailureException

@Transactional
class DstvService {

    def getPackage(String packageName) {
        /* Price, Channels */
        System.console().readLine()
    }

    def getChannel(int channelNumber) {
        /* Name, Number, ;Packages with it, Category, Similar channels (5) */
    }

    JSONObject getPackageJson(String packageName, DstvCountry country) {
        packageName = packageName?.replace(" ", "-")?.toLowerCase()
        def now = new Date()
        def isRefreshedRecently = isRefreshedRecently(now, DstvPackage.findBySlugAndCountry(packageName, country))
        JSONObject result = null

        if (!country) {
            log.info("Country cannot be null to retrieve package $packageName information. Skipping...")
            return null
        }

        if (isRefreshedRecently) {
            log.info("$packageName for $country already refreshed within the last 7 days. Skipping...")
            return null
        }

        String url = "https://eazy.dstv.com/$country.defaultLanguageCode/$country.twoLetterCode/get-dstv/package/details/dstv-${java.net.URLEncoder.encode(packageName, "UTF-8")}"
        String pageContent = null

        try {
            pageContent = new URL(url)
                    .getText(connectTimeout: 5000,
                    readTimeout: 10000,
                    useCaches: true,
                    allowUserInteraction: false,
                    requestProperties: ['Connection': 'close',
                                        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'])
        } catch (IOException ex) {
            /* TODO: Put a condition that breaks the recursion, if the exception happens consecutively up to a certain number of times */
            log.error("There was a problem connecting to DSTV to get package information for $packageName in $country. Trying again in 5....")
            Thread.sleep(5000)
            return getPackageJson(packageName, country)
        }

        def matcher = pageContent =~ /(\{"user":.+\}\})\),/
        if (matcher.size() == 1 && (matcher[0] as ArrayList)?.size() == 2) {
            result = JSON.parse(matcher[0][1] as String) as JSONObject
        } else{
            log.info("The response for $packageName $country did not fit the format. Returning null")
        }

        result
    }

    def refreshCountryPackage(String packageName, DstvCountry country) {
        packageName = packageName?.replace(" ", "-")?.toLowerCase()
        def now = new Date()
        def isRefreshedRecently = isRefreshedRecently(now, DstvPackage.findBySlugAndCountry(packageName, country))

        if (country && !isRefreshedRecently) {
            String url = "https://eazy.dstv.com/$country.defaultLanguageCode/$country.twoLetterCode/get-dstv/package/details/dstv-$packageName"
            String pageContent = null

            try {
                pageContent = new URL(url)
                        .getText(connectTimeout: 5000,
                        readTimeout: 10000,
                        useCaches: true,
                        allowUserInteraction: false,
                        requestProperties: ['Connection': 'close',
                                            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'])
            } catch (IOException ex) {
                /* TODO: Put a condition that breaks the recursion, if the exception happens consecutively up to a certain number of times */
                log.error("There was a problem connecting to DSTV to get package information for $packageName in $country. Trying again in 5....")
                Thread.sleep(5000)
                return refreshCountryPackage(packageName, country)
            }

            def matcher = pageContent =~ /(\{"user":.+\}\})\),/
            if (matcher.size() == 1 && (matcher[0] as ArrayList)?.size() == 2) {
                def packageJson = JSON.parse(matcher[0][1] as String) as JSONObject
                log.info("Successfully saved package: ${savePackage(packageName, packageJson, country)?.name} for $country")
            }
        } else {
            /* TODO: Return country is not available yet error; List available countries */
        }
    }

    def isRefreshedRecently(Date time, DstvPackage aPackage) {
        def isRefreshedRecently = false

        use(TimeCategory) {
            isRefreshedRecently = aPackage?.lastRefreshDate > time - 7.days
            /* Has the package been refreshed within the past 7 days */
        }
    }

    DstvChannel trySaveChannel(String name, int number, String languageCode) {
        DstvChannel.findOrCreateWhere(
                [
                        name        : name,
                        number      : number,
                        languageCode: languageCode
                ])
    }

    def savePackage(String packageSlug, JSONObject thePackage, DstvCountry theCountry) {
        if (thePackage) {
            JSONObject innerPackage = thePackage.package
            def savedPackage = DstvPackage.findOrCreateByNameAndCountry(innerPackage.Product.Name, theCountry)
            def now = new Date()

            def isRefreshedRecently = isRefreshedRecently(now, savedPackage)

            if (isRefreshedRecently) {
                log.info("$savedPackage already refreshed within the last 7 days. Skipping...")
                return
            }

            savedPackage.price = innerPackage.Product.Price
            savedPackage.logo = innerPackage.Product.Logo
            savedPackage.thumbnail = innerPackage.Product.Thumbnail
            savedPackage.description = innerPackage.Product.Description
            savedPackage.slug = packageSlug
            savedPackage.urlFriendlyName = packageSlug
                    .replace("é", "e") /* specifically to tackle DSTV bué package; not the best solution TBH */
                    .replace("á", "a") /* specifically to tackle DSTV fácil package; not the best solution TBH */
                    .toLowerCase()
                    .trim()

            innerPackage.ChannelsByGenre.each { category ->
                category.ChannelList.each { channel ->
                    /* TODO: Check if channel is already in package. Yes, */

                    def savedChannel = trySaveChannel(channel.ChannelName, channel.ChannelNumber as int, theCountry.defaultLanguageCode)

                    def changed = false

                    if(savedChannel.description != channel.ChannelDescription){
                        changed = true
                        savedChannel.description = channel.ChannelDescription
                    }

                    if(savedChannel.category != channel.GenreDescription){
                        changed = true
                        savedChannel.category = channel.GenreDescription
                    }

                    if(savedChannel.mobileLogo != channel.MobileLogo){
                        changed = true
                        savedChannel.mobileLogo = channel.MobileLogo
                    }

                    if(savedChannel.webLogo != channel.WebLogo){
                        changed = true
                        savedChannel.webLogo = channel.WebLogo
                    }

                    String urlFriendlyName = savedChannel.name
                            .replaceAll("[^A-Za-z0-9]", "-")
                            .replaceAll("\\s+", "-")
                            .trim()
                            .toLowerCase()

                    if(savedChannel.urlFriendlyName != urlFriendlyName){
                        changed = true
                        savedChannel.urlFriendlyName = urlFriendlyName
                    }

                    if(changed){
                        savedChannel.save(failOnError: true)
                    }

                    if(!savedPackage.channels || !savedPackage.channels.contains(savedChannel))
                        savedPackage.addToChannels(savedChannel)
                }
            }

            savedPackage.lastRefreshDate = now
            savedPackage.save(failOnError: true, flush: true)
        } else {
            log.info("No package information was returned for package: $packageSlug in $theCountry")
        }
    }

    def getCountryPackageNames(DstvCountry country) {
        def packages = []
        if (country) {
            String url = "https://eazy.dstv.com/$country.defaultLanguageCode/$country.twoLetterCode/get-dstv/packages"
            String pageContent = null

            try {
                pageContent = new URL(url)
                        .getText(connectTimeout: 5000,
                        readTimeout: 10000,
                        useCaches: true,
                        allowUserInteraction: false,
                        requestProperties: ['Connection': 'close', 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'])

            } catch (IOException ex) {
                /* TODO: Put a condition that breaks the recursion, if the exception happens consecutively up to a certain number of times */
                log.error("There was a problem connecting to DSTV to get package names for $country. Trying again in 5....")
                Thread.sleep(5000)
                return getCountryPackageNames(country)
            }

            def matcher = pageContent =~ /href="package\/details\/dstv-([^"]+)"/
            def matchSize = matcher.size()
            if (matchSize) {
                matcher.each { matchParent ->
                    def packageName = matchParent[1]
                    if (!packages.contains(packageName)) {
                        packages.add(packageName)
                    }
                }
            }
        } else {
            /* TODO: Return country is not available yet error; List available countries */
        }
        packages
    }

    def getAllCountryPackages() {

    }

    def categorizeChannels(Set<DstvChannel> channels){
        def theCategories = []
        channels.each { DstvChannel channel ->
            def theCategory = theCategories.find { it.name == channel.category }
            if(theCategory){
                theCategory.channels << channel
            } else{
                def newCategory = [ name: channel.category, channels: [ channel ]]
                theCategories << newCategory
            }
        }
        theCategories.sort{
            it.name
        }
        theCategories.each {
            it.channels = it.channels.sort{ it.name }
        }
        theCategories
    }

    /*def generateChannelInCountryPackageUrls(String root)
    {
        def countries = DstvCountry.list()
        countries.each { theCountry ->
            def urls = []
            def channels = theCountry.countryChannels
            def packages = theCountry.countryPackages

            channels.each { theChannel ->
                packages.each { thePackage ->
                    urls << "$root/does-dstv-$thePackage.urlFriendlyName-have-$theChannel.urlFriendlyName-channel-num-$theChannel.number-$theCountry.urlFriendlyName"
                }
            }
            File lstFile = new File("sitemap.txt")
            lstFile.withWriterAppend{ out ->
                urls.each {out.println it}
            }
        }
    }*/
}
