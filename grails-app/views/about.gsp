<!doctype html>
<html>
<head>
    <title>About</title>
    <meta name="layout" content="main">
</head>

<body>
<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">About <g:message code="application.name"></g:message></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    About
                </div>

                <div class="panel-body">
                    <div>
                        <g:message code="application.name"/> is a search engine created to help DSTV subscribers around Africa to find the DSTV package/bouquet
                        that best suits their needs. The idea was born one day when the creator found out that his family constantly watched only 10
                        of the over 100 channels on the DSTV Nigeria Compact bouquet which he's been constantly subscribed to for the past few years.
                        This discovery presented an opportunity to cut down monthly spend on DSTV subscription by downgrading to a cheaper package that
                        still had most or all of the 10 favorite channels.
                    </div>
                    <br>
                    <div>
                        The creator hit the Google search engine and alas, realized that there was no straightforward way of getting enough
                        information to choose a better package. Of course, by clicking through several web pages, he eventually found the answer
                        to his inquiries. However, he concluded that this experience could be better and decided to do something about it so
                        other people would not face same trouble he just did.
                    </div>
                    <br>
                    <div>
                        As fate would have it, the creator happens to be a programmer but since he does not work in DSTV where he could
                        re-design the website to achieve this, he decided he was going to take it up as a personal project. As is clear to see,
                        he lived up to his word, so here you are
                    </div>
                </div>

                <div class="panel-footer">
                    <g:link mapping="howItWorks">
                        How does <g:message code="application.name"/> work?
                    </g:link>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>