<%--
  Created by IntelliJ IDEA.
  User: Hafiz
  Date: 7/24/2017
  Time: 12:14 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Does ${thePackage} package in ${country} have ${theChannel} Channel ${theChannel.number}</title>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>
</head>

<body>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">DSTV package search result</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row" id="response-display">
        <div class="col-xs-12 text-center">
            <div class="jumbotron">
                <p id="title">
                    Does ${thePackage} package in ${country} have <strong
                        class="text-primary">${theChannel} Channel ${theChannel.number}</strong>?
                </p>
                <g:if test="${searchResponse == 'YES'}">
                    <div class="text-primary">
                        <h1>${searchResponse}</h1>
                    </div>

                    <p>
                        Below is more information on all DSTV ${country} packages which have <strong
                            class="text-primary">${theChannel} Channel ${theChannel.number}</strong>
                    </p>
                </g:if>
                <g:else>
                    <div class="text-danger">
                        <h1>${searchResponse}</h1>
                    </div>

                    <p>
                        Not to worry. Below are all the DSTV ${country} packages which have ${theChannel} Channel ${theChannel.number}
                    </p>
                </g:else>
                <small>** Click the package name to see even more information about the package</small>
            </div>
        </div>

    </div>

    <div class="row" id="channel-packages">
        <table>
            <thead>
            <tr>
                <th>Package</th>
                <th>Cost (${country.currency})</th>
                <th>Number of channels</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${channelPackages}" var="aPackage">
                <tr>
                    <td>
                        <g:link mapping="packageChannelsAndInfo"
                                params="[
                                        countryName: country.urlFriendlyName,
                                        packageName: aPackage.urlFriendlyName
                                ]">
                            ${aPackage}
                        </g:link>
                    </td>
                    <td>${aPackage.price}</td>
                    <td>${aPackage.channels.size()}</td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>