<%--
  Created by IntelliJ IDEA.
  User: Hafiz
  Date: 7/27/2017
  Time: 8:56 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>DSTV ${theCountry} - ${theChannel} Channel ${theChannel.number} packages and similar</title>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>
</head>

<body>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">DSTV Channel Information</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row" id="title">
        <div class="col-xs-12 text-center">
            <div class="jumbotron">
                <h1>
                    DSTV ${theChannel} Channel ${theChannel.number}
                </h1>

                <p>
                    ${theChannel?.descriptionSanitized?.decodeHTML()}
                </p>
                <small>**DSTV ${theChannel} Channel ${theChannel.number} is in the ${theChannel.category} category</small>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    DSTV ${theChannel} Channel ${theChannel.number} information
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#packages" data-toggle="tab" aria-expanded="true">Packages</a>
                        </li>
                        <li class=""><a href="#similar-channels" data-toggle="tab"
                                        aria-expanded="false">Similar channels</a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="packages">
                            <h1>DSTV ${theCountry} packages with ${theChannel} Channel ${theChannel.number}</h1>

                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>Package</th>
                                        <th>Cost (${theCountry.currency})</th>
                                        <th>Number of channels</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <g:each in="${channelPackages}" var="aPackage">
                                        <tr>
                                            <td>
                                                <g:link mapping="packageChannelsAndInfo"
                                                        params="[
                                                                countryName: theCountry.urlFriendlyName,
                                                                packageName: aPackage.urlFriendlyName
                                                        ]">
                                                    ${aPackage}
                                                </g:link>
                                            </td>
                                            <td>${aPackage.price}</td>
                                            <td>${aPackage.channels.size()}</td>
                                        </tr>
                                    </g:each>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                        <div class="tab-pane fade" id="similar-channels">
                            <h1>DSTV ${theCountry} channels similar to ${theChannel} Channel ${theChannel.number}</h1>
                            <g:each in="${similarChannels}" var="aChannel">
                                <p>
                                    <g:link mapping="channelPackagesAndInfo"
                                            params="[
                                                    channelName  : aChannel.urlFriendlyName,
                                                    channelNumber: aChannel.number,
                                                    countryName  : theCountry.urlFriendlyName
                                            ]">
                                        ${aChannel.name} Channel ${aChannel.number}
                                    </g:link>
                                </p>
                            </g:each>
                        </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
</div>
</body>
</html>