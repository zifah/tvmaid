<%--
  Created by IntelliJ IDEA.
  User: Hafiz
  Date: 7/28/2017
  Time: 10:37 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>DSTV ${theCountry} - Packages and channels</title>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>
</head>

<body>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">DSTV ${theCountry} Packages &amp; Channels</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row" id="title">
        <div class="col-xs-12 text-center">
            <div class="jumbotron">
                <h1>
                    DSTV ${theCountry}
                </h1>

                <p>
                    ${thePackages.size()} Packages; ${theChannels.size()} channels
                </p>
            </div>
        </div>
    </div>

    <div class="row" id="country-information">

        <div class="row">
            <div class="col-sm-12 col-md-4" id="country-packages">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        DSTV ${theCountry} Packages
                    </div>

                    <div class="panel-body">
                        <g:each in="${thePackages}" var="aPackage">
                            <p>
                                <g:link mapping="packageChannelsAndInfo"
                                        params="[
                                                countryName: theCountry.urlFriendlyName,
                                                packageName: aPackage.urlFriendlyName
                                        ]">
                                    ${aPackage} (${aPackage?.channels?.size()} channels | ${theCountry?.currency} ${aPackage?.price})
                                </g:link>
                            </p>
                        </g:each>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-8" id="country-channels">
                <g:each in="${theChannels}" var="aChannel">
                    <div class="panel panel-default">
                        <div class="panel-heading">

                            <g:link mapping="channelPackagesAndInfo"
                                    params="[
                                            channelName  : aChannel.urlFriendlyName,
                                            channelNumber: aChannel.number,
                                            countryName  : theCountry.urlFriendlyName
                                    ]">${aChannel} Channel ${aChannel.number}  in DSTV ${theCountry}</g:link>
                        </div>

                        <div class="panel-body">
                            <h4>
                                ${aChannel?.descriptionSanitized?.decodeHTML()}
                            </h4>

                            <p>
                                <g:link mapping="channelPackagesAndInfo"
                                        params="[
                                                channelName  : aChannel.urlFriendlyName,
                                                channelNumber: aChannel.number,
                                                countryName  : theCountry.urlFriendlyName
                                        ]">
                                    Click here to see which DSTV packages have ${aChannel} Channel ${aChannel.number}
                                </g:link>
                            </p>
                        </div>
                    </div>
                </g:each>
            </div>
        </div>
    </div>
</div>
</body>
</html>