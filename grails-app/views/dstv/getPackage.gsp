<%--
  Created by IntelliJ IDEA.
  User: Hafiz
  Date: 7/26/2017
  Time: 10:54 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${thePackage} prices and channels in ${theCountry}</title>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>
</head>

<body>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">${thePackage} ${theCountry} information</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row" id="title">
        <div class="col-xs-12 text-center">
            <div class="jumbotron">
                <h1>
                    The price for one month of <strong
                        class="text-primary">${thePackage}</strong> in ${theCountry} is <strong
                        class="text-primary">${theCountry.currency} ${thePackage.price}</strong>. It has <strong
                        class="text-primary">${thePackage.channels.size()}</strong> channels
                </h1>
            </div>
        </div>
    </div>

    <div class="row" id="package-information">

        <div class="row">
            <div class="col-sm-12 col-md-4" id="package-categories">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        ${thePackage} ${theCountry} Channel Categories
                    </div>

                    <div class="panel-body">
                        <g:each in="${theCategories}" var="aCategory">
                            <p>
                                <a href="#${aCategory.name}">${aCategory.name} (${aCategory.channels.size()})</a>
                            </p>
                        </g:each>

                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-8">
                <g:each in="${theCategories}" var="aCategory">
                    <div class="panel panel-primary" id="${aCategory?.name}">
                        <div class="panel-heading">
                            ${aCategory?.name} channels in ${thePackage} ${theCountry}
                        </div>

                        <div class="panel-body">

                            <g:each in="${aCategory.channels}" var="aChannel">
                                <p>
                                    <g:link mapping="channelPackagesAndInfo"
                                            params="[
                                                    channelName  : aChannel.urlFriendlyName,
                                                    channelNumber: aChannel.number,
                                                    countryName  : theCountry.urlFriendlyName
                                            ]">
                                        ${aChannel.name} Channel ${aChannel.number}
                                    </g:link>
                                </p>
                            </g:each>

                        </div>
                    </div>
                </g:each>
            </div>
        </div>
    </div>
</div>
</body>
</html>