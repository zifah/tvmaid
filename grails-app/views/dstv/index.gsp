<%--
  Created by IntelliJ IDEA.
  User: Hafiz
  Date: 7/28/2017
  Time: 11:25 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <meta name="google-site-verification" content="yWUfJIs6a372v40mszYC6GzjUnQ1dXkYum4j8D9J_S0"/>
    <title>Choose the right DSTV package</title>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>
</head>

<body>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Home</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row" id="title">
        <div class="col-lg-12">
            <div class="jumbotron">
                <div class="col-md-12 col-lg-8">
                    <g:select name="country" from="${theCountries}"
                              optionKey="${{
                                  createLink(mapping: "countryPackagesAndChannels", params: [countryName: it.urlFriendlyName])
                              }}"
                              noSelection="['': '- Where are you? -']" class="form-control"/>
                </div>

                <div class="col-md-12 col-lg-4">
                    <a id="country-link" class="btn btn-primary">GO!</a>
                </div>
            </div>
        </div>

    </div>
</div>
<asset:javascript src="jquery-2.2.0.min.js"/>
<script type="text/javascript">
    $("select#country").change(function () {
        var countryEndpoint = this.value;
        $("a#country-link").attr("href", countryEndpoint);
    });
</script>
</body>
</html>