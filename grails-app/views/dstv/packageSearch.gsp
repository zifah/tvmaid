<%--
  Created by IntelliJ IDEA.
  User: Hafiz
  Date: 7/24/2017
  Time: 12:14 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Does DSTV ${country} package have channels</title>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>
</head>

<body>
<div id="page-wrapper" ng-app="tvMaidApp" ng-controller="tvMaidCtrl">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header" title="">DSTV ${country} Channel-Package search
                <i title="Check if a DSTV channel (or group of channels) is in a particular DSTV ${country} package"
                   class="glyphicon glyphicon-question-sign"></i></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Search for DSTV channels in a DSTV ${country} package
                </div>
                <!-- .panel-heading -->
                <div class="panel-body">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#search-area"
                                       aria-expanded="${channelPackageStatuses == null}">Search Area</a>
                                </h4>
                            </div>

                            <div id="search-area"
                                 class="panel-collapse collapse${channelPackageStatuses == null ? ' in' : ''}"
                                 aria-expanded="${channelPackageStatuses == null}"
                                 style="${channelPackageStatuses != null ? 'height: 0px;' : ''}">
                                <div class="panel-body">
                                    <div class="row">
                                        <g:form mapping="packageSearch" params="[countryName: country.urlFriendlyName]"
                                                role="form">
                                            <div class="row">

                                                <div class="form-group">
                                                    <label>PACKAGE</label>
                                                    <g:select name="package" from="${packageList}"
                                                              optionValue="name" optionKey="id"
                                                              noSelection="['': '-Select Package-']"
                                                              class="form-control"/>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-md-6">

                                                    <div class="form-group">
                                                        <label>SELECT CHANNELS</label>

                                                        <div class="row">
                                                            <div class="col-md-12 col-lg-8" style="
                                                            max-height: 500px;
                                                            overflow-y: auto;
                                                            overflow-x: hidden;">
                                                                <g:select name="channels" from="${channelList}"
                                                                          optionValue="name" optionKey="id"
                                                                          noSelection="['': '-Select Channel-']"
                                                                          class="form-control"/>
                                                            </div>

                                                            <div class="col-md-12 col-lg-4">
                                                                <button class="btn btn-primary form-control"
                                                                        ng-click="addSelectedChannel()"
                                                                        type="button">ADD</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-xs-12 col-md-6">
                                                    <h4>Channels to check</h4>

                                                    <p ng-repeat="channel in selectedChannels">
                                                        <label><input type="checkbox" name="selectChannels"
                                                                      value="{{channel.id}}"
                                                                      checked="checked"/> {{channel.name}}</label>
                                                    </p>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-xs-2 col-md-4"></div>

                                                <div class="col-xs-2 col-md-4"></div>

                                                <div class="col-xs-8 col-md-4">

                                                    <button class="btn btn-success form-control"
                                                            type="submit">SEARCH</button>
                                                </div>
                                            </div>
                                        </g:form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#search-results"
                                       aria-expanded="${channelPackageStatuses != null}">View results</a>
                                </h4>
                            </div>

                            <div id="search-results" class="panel-collapse collapse in"
                                 aria-expanded="${channelPackageStatuses != null}"
                                 style="${channelPackageStatuses == null ? 'height: 0px;' : ''}">
                                <div class="panel-body">
                                    <g:if test="${channelPackageStatuses}">
                                        <div class="row" id="channels-package-state">
                                            <div class="col-xs-12">
                                                <p>Results for DSTV channels in <strong
                                                        class="text-primary">${packageName}</strong>. Please, click the DSTV channel name below to see all DSTV packages which have it
                                                </p>
                                                <table>
                                                    <tbody>
                                                    <g:each in="${channelPackageStatuses}" var="status">
                                                        <tr>
                                                            <td>
                                                                Does ${packageName} have
                                                                <g:link mapping="channelInPackage"
                                                                        params="[
                                                                                channelName  : status.urlBuilder.channelName,
                                                                                channelNumber: status.urlBuilder.channelNumber,
                                                                                countryName  : country.urlFriendlyName,
                                                                                packageName  : status.urlBuilder.packageName
                                                                        ]">
                                                                    ${status.channelName} Channel ${status.channelNumber}?
                                                                </g:link>
                                                            </td>
                                                            <td><strong
                                                                    class="${status.status == "YES" ? 'text-success' : 'text-danger'}">${status.status}</strong>
                                                            </td>
                                                        </tr>
                                                    </g:each>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </g:if>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
    <!-- Angular JS -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.1/angular.min.js"></script>
    <asset:javascript src="app.js"/>
</div>
</body>
</html>