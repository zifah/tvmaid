<!doctype html>
<html>
<head>
    <title>How it works</title>
    <meta name="layout" content="main">
    <style>
    li > ul > li {
        margin-left: 15px;
    }

    li > ol > li {
        margin-left: 15px;
    }
    </style>
</head>

<body>
<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">How <g:message code="application.name"></g:message> works</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    User guide
                </div>
                <!-- .panel-heading -->
                <div class="panel-body">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseOne">Search a DSTV package for channels</a>
                                </h4>
                            </div>

                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <ol>
                                        <li>Navigate to the <a href="${createLink(uri: '/')}"><g:message code="application.name"/> home page</a>
                                        </li>
                                        <li>Choose your DSTV country and click GO.</li>
                                        <li>
                                            On the page that opens, select "Search Package" from the menu at the top of the page. This will take you to the DSTV package search page</li>
                                        <li>
                                            On the search page:
                                            <ul>
                                                <li>
                                                    Select the DSTV package within which you want to search
                                                </li>
                                                <li>In the Select Channels dropdown, choose a channel and click the blue "Add" button. The DSTV channel will be added to a list. You can add as many DSTV channels as you want</li>
                                                <li>Once you've added all your desired DSTV channels, click the green "Search" button and wait for a few seconds</li>
                                                <li>You will be presented with a table which contains information on the availability of each selected DSTV channel in the selected DSTV package in a simple YES or NO format</li>
                                                <li>You can click the channel name in the results table to see all the DSTV packages which have that channel. This is particularly useful in the case that the selected package does not have the channel</li>
                                            </ul>
                                        </li>
                                    </ol>

                                    <p>
                                        <a href="#">Send feedback</a>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseTwo">Peek into a DSTV Channel</a>
                                </h4>
                            </div>

                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ol>
                                        <li>Navigate to the <a href="${createLink(uri: '/')}"><g:message code="application.name"/> home page</a>
                                        </li>
                                        <li>Choose your DSTV country and click <strong>GO</strong>. You will be redirected to a page which contains a list
                                        and short description of all the DSTV channels in your country</li>
                                        <li>
                                            Just click the name of any DSTV channel e.g. Nat Geo Wild Channel 182 to see the following information about it:
                                            <ul>
                                                <li>
                                                    DSTV packages which have the channel
                                                </li>
                                                <li>Other DSTV channels like the channel</li>
                                            </ul>
                                        </li>
                                    </ol>

                                    <p>
                                        <a href="#">Send feedback</a>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseThree">Peek into a DSTV Package</a>
                                </h4>
                            </div>

                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Follow the steps below to see the <strong>Price</strong> of, <strong>Number of channels</strong>, <strong>Categories</strong>
                                        and <strong>Channels</strong> in a DSTV package.</p>
                                    <ol>
                                        <li>Navigate to the <a href="${createLink(uri: '/')}"><g:message code="application.name"/> home page</a>
                                        </li>
                                        <li>Choose your DSTV country and click <strong>GO</strong>. You will be redirected to a page which contains a list of all
                                        the DSTV Packages in your country</li>
                                        <li>
                                            Select any DSTV package in the list to see:
                                            <ul>
                                                <li>the price of the DSTV package (in your country's currency);</li>
                                                <li>the number of channels in the DSTV package;</li>
                                                <li>a list of all the categories of channels in that DSTV package. Click any category name to instantly see a list
                                                of all the DSTV package's channels in that category</li>
                                             </ul>
                                        </li>
                                    </ol>

                                    <p>
                                        <a href="#">Send feedback</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
</body>
</html>