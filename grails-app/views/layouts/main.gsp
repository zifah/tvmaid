<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Grails"/> | <g:message code="application.name"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <asset:stylesheet src="application.css"/>

    <g:layoutHead/>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-3786592731920031",
            enable_page_level_ads: true
        });
    </script>
</head>

<body>
<g:set var="theCountry" value="${theCountry ?: country}"/>
<div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/#">
                <i class="fa grails-icon">
                    <asset:image src="grails-cupsonly-logo-white.svg"/>
                </i> <g:message code="application.name" />: <small>${theCountry ? theCountry : "Choose the right DSTV package"}</small>
            </a>
        </div>

        <div class="navbar-collapse collapse" aria-expanded="false" style="height: 0.8px;">
            <ul class="nav navbar-nav navbar-right">
                <g:pageProperty name="page.nav"/>
                <li>
                    <a href="${createLink(mapping: 'about')}">About</a>
                </li>
                <li>
                    <a href="${createLink(mapping: 'howItWorks')}">How it works</a>
                </li>
                <g:if test="${theCountry || country}">
                    <li>
                        <a href="${createLink(mapping: 'packageSearch', params: [countryName: theCountry.urlFriendlyName])}"
                           role="link">Search Package </a>
                    </li>
                    <li>
                        <a href="${createLink(mapping: 'countryPackagesAndChannels', params: [countryName: theCountry.urlFriendlyName])}"
                           role="link"> All DSTV Packages </a>
                    </li>
                    <li>
                        <a href="#">Feedback</a>
                    </li>
                </g:if>
            </ul>
        </div>
        </div>
    </div>
</div>
<g:if test="${flash.error}">
    <div class="alert alert-danger" role="status">${flash.error}</div>
</g:if>
<g:if test="${flash.message}">
    <div class="alert alert-info" role="status">${flash.message}</div>
</g:if>
<g:layoutBody/>

<div class="footer" role="contentinfo">
    <div class="text-center">
        Built with love by <a href="http://www.hafiz.com.ng" target="_blank" class="btn btn-default">Hafiz Adewuyi</a>. This is not DSTV website
    </div>
</div>

<div id="spinner" class="spinner" style="display:none;">
    <g:message code="spinner.alt" default="Loading&hellip;"/>
</div>

<asset:javascript src="application.js"/>

</body>
</html>
