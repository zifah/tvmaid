package nse

/**
*  Created by Hafiz on 10/2/2017.
*/
public enum TradeType {
    CorrectionRemove(-1), Sell(0), Buy(1), CorrectionAdd(2)

    TradeType(int value) {
        this.value = value
    }

    private final int value

    int getValue() {
        value
    }
}
