package tvmaid

import grails.util.Holders
import org.apache.commons.logging.LogFactory
import org.grails.web.json.JSONObject

/**
 * Created by Hafiz on 7/23/2017.
 */
class DstvPackageRefresher implements Runnable {
    DstvCountry country
    String packageName
    JSONObject result
    DstvService dstvService = Holders.grailsApplication.mainContext.getBean('dstvService')
    def done
    def saved

    private static final log = LogFactory.getLog(this)


    DstvPackageRefresher(DstvCountry country, String packageName) {
        this.country = country
        this.packageName = packageName
    }

    void run() {
        log.info("Start: Getting packages for $packageName $country")
        result = dstvService.getPackageJson(packageName, country)
        done = true
        log.info("Done: Getting packages for $packageName $country")
    }

}
