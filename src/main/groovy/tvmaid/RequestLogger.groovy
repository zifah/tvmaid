package tvmaid

import groovyx.gpars.actor.DynamicDispatchActor
import org.springframework.transaction.TransactionStatus

/**
 * Created by Hafiz on 7/30/2017.
 */
final class RequestLoggerActor extends DynamicDispatchActor {
    void onMessage(def theMap) {
        def theLog = new RequestLog(
                clientIp: theMap.clientIp,
                theUrl: theMap.uri,
                startTime: theMap.startTime,
                responseCode: theMap.responseCode,
                userAgent: theMap.userAgent,
                isMobile: theMap.isMobile
        )
        theLog.latencyMs = (new Date()).getTime() - theLog.startTime.getTime()
        RequestLog.withTransaction { TransactionStatus status ->
            theLog.save(flush: true)
        }
        true
    }
}
