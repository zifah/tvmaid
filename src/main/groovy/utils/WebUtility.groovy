package utils
/**
 * Created by Hafiz on 10/2/2017.
 */
class WebUtility {
    static def makeGetRequest(String url){
        new URL(url)
                .getText(connectTimeout: 5000,
                readTimeout: 10000,
                useCaches: true,
                allowUserInteraction: false,
                requestProperties: ['Connection': 'close',
                                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) ' +
                                            'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'])
    }
}
