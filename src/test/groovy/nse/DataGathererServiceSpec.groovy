package nse

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(DataGathererService)
@Mock([StockDailyTradeData, StockCompany])
class DataGathererServiceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
        when:
        service.collectTodaysTradeData(new Date().clearTime())
        then:
        true
    }
}
