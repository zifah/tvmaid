package tvmaid


import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(DstvInterceptor)
class DstvInterceptorSpec extends Specification {

    def setup() {
    }

    def cleanup() {

    }

    void "Test dstv interceptor matching"() {
        when:"A request matches the interceptor"
            withRequest(controller:"dstv")

        then:"The interceptor does match"
            interceptor.doesMatch()
    }
}
